asgiref==3.4.1
Django==3.2.5
pytz==2019.1
sqlparse==0.4.1
pip==21.1.3