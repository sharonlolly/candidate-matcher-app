from django.shortcuts import render

from .models import Job
from .candidate_finder import candidate_finder
import logging

logger = logging.getLogger(__name__)
# Create your views here.


def index(request):
    jobs = Job.objects.all()
    logger.info("All Jobs in the database are:\n{}".format(jobs))
    return render(request, 'candidate_matcher/index.html', {
        'jobs': jobs
    })


def candidate_matcher_details(request, job_slug):
    job_obj = Job.objects.get(slug=job_slug)
    logger.info("Calculating matched candidates for job: {}".format(job_obj))
    candidates = candidate_finder(job_obj)
    if candidates:
        logger.info("Matched candidates:\n {}".format(candidates))
        return render(request, 'candidate_matcher/candidate_matcher-details.html', {
                'candidates_found': True,
                'candidates': candidates
            })
    else:
        return render(request, 'candidate_matcher/candidate_matcher-details.html', {
            'candidates_found': False
        })
