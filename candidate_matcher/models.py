from django.db import models

# Create your models here.


class Skill(models.Model):
    skill_name = models.CharField(max_length=200, unique=True)
    value = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        skill_as_str = "{}, skill_value: {}".format(self.skill_name, self.value)
        return skill_as_str


class Candidate(models.Model):
    candidate_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    skills = models.ManyToManyField(Skill)

    def __str__(self):
        candidate_as_str = "title: {}, candidate skills: {}".format(self.title, list(self.skills.all()))
        return candidate_as_str


class Job(models.Model):
    job_id = models.AutoField(primary_key=True)
    slug = models.SlugField(unique=True)
    title = models.CharField(max_length=200)
    skill = models.CharField(max_length=200)

    def __str__(self):
        job_as_str = "Job title: {}, Job skill: {}".format(self.title, self.skill)
        return job_as_str
