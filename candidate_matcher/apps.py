from django.apps import AppConfig


class CandidatesMatcherConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'candidate_matcher'
