from django.urls import path
from . import views


urlpatterns = [
  path('', views.index, name='all-jobs'),  # our-domain.com/candidate_matcher
  path('<slug:job_slug>', views.candidate_matcher_details, name='job-detail'),  # our-domain.com/candidate_matcher/<dynamic-path-segment>
]
