from django.db.models import Case, IntegerField, When, Sum, Q
from candidate_matcher import models
import logging

logger = logging.getLogger(__name__)


def rank_final_candidates(potential_candidates):
    """
    This function is ranking all candidates that have been matched,
    based on the sum of their skills value.
    For example:
    for 2 potential candidates:
        Electrical Engineer, with skills: Python, Math - sum of skills values = 20
        Software Developer, with skills: Python, Javascript, Postgres - sum of skills values = 30

    their order will be as follows:
        1) Software Developer
        2) Electrical Engineer
    """
    list_of_ids = list(map(lambda candidate: candidate.candidate_id, potential_candidates))
    potential_candidates = models.Candidate.objects.filter(pk__in=list_of_ids)
    logger.info("Potential Candidates as query set:\n {}".format(potential_candidates))
    final_candidates = potential_candidates.annotate(
        skill_total=Sum('skills__value'),
        o=Case(
            When(skill_total__isnull=True, then=0),
            default=1,
            output_field=IntegerField()
        )
    ).order_by('-o', '-skill_total')
    logger.info("Final ranked Candidates as query set:\n {}".format(final_candidates))
    return potential_candidates


def get_matches_from_job_title(job_obj):
    """check for identical or partial match with certain substring (ex: Software Engineer / Software Developer)"""
    job_title = models.Job.objects.get(job_id=job_obj.job_id).title
    logger.info("Get matches base on job title: {}".format(job_title))
    title_list = job_title.split()
    if len(title_list) == 2:  # naive approach - assuming all titles are max 2 words
        query_set_titles = models.Candidate.objects.filter(
            (Q(title__icontains=title_list[0]) | Q(title__icontains=title_list[1])))
    else:
        query_set_titles = models.Candidate.objects.filter(title=job_title)
    logger.info("matches base on job title: {}".format(query_set_titles))
    return query_set_titles


def get_matches_from_skill(job_obj):
    """checks for candidates who have the skill required for the job"""
    skill_to_check = job_obj.skill.capitalize()
    logger.info("Get matches base on job skill: {}".format(skill_to_check))
    skill_id = models.Skill.objects.get(skill_name=skill_to_check).id
    query_set_skills = models.Candidate.objects.filter(skills=skill_id)

    logger.info("matches base on job skill: {}".format(query_set_skills))
    return query_set_skills


def candidate_finder(job_obj):
    """utility function to evaluate matches for given job_id"""
    ranked_candidates = []
    try:
        # for a more accurate approach -> implement levenshtein distance algorithm
        matching_title_candidates = get_matches_from_job_title(job_obj)
        matching_skills_candidates = get_matches_from_skill(job_obj)

        # ideal candidates will match with both title and skill
        potential_candidates = list(set(matching_title_candidates) & set(matching_skills_candidates))
        if not potential_candidates:  # if there are no matches for both skills - try taking candidates with only one
            potential_candidates = list(set(matching_title_candidates) | set(matching_skills_candidates))
        logger.info("potential candidates: {}".format(potential_candidates))
        # rank the final candidates based on how strong the match is to the job
        ranked_candidates = rank_final_candidates(potential_candidates)
        logger.info("ranked candidates: {}".format(ranked_candidates))
    except Exception as e:
        logger.error(f'Error getting all matching candidates: {e}')

    return ranked_candidates
