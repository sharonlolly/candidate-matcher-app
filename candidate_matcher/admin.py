from django.contrib import admin
from .models import Candidate, Job, Skill


# Register your models here.


admin.site.register(Candidate)
admin.site.register(Job)
admin.site.register(Skill)
