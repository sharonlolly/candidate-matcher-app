# Candidate Matcher

## About 
This is an implementation of a basic “matcher” application. The application has 3
main objects: Skill, Candidate, Job. The application’s core function is “CandidateFinder” which given a job, returns the best
candidates for this job.


## Installing / Running Locally
* Install requirements
 
       pip install -r requirements.txt
       
* Make migrations / migrate tables:
       
       python manage.py makemigrations
       python manage.py migrate

* Reading initial data into the database:
        
       python manage.py loaddata skills.json jobs.json candidates.json

* Running server:
       
       python manage.py runserver

* open the app at path:

       <local-domain>/candidate_matcher/


## Candidate Matching 

The naive approach taken in this application checks for a match based on title matching (or some substring
of the title matching) as well as a skills match.

A more advanced approach for this use case would be to use fuzzy matching. Since the goal is to link some text
(candidate job title / skills) with the target job title / skill, we can try to identify non-exact matches
between the two.

We can do this by calculating the Levenshtein Distance representing the number of transformations required to get
from the source string to the target string, with a lower distance representing a higher match probability.

The downside to this approach is that it does not account for relevance, and misspellings / typos could completely
transform the meaning of the word. This would be problematic when comparing "Developer" to "Engineer".
Even though the two words are very similar job wise, the Levenshtein distance would be large between the two.

# Ranking the Candidates
Currently, The app is ranking all candidates that have been matched based on the sum of their skills value.

For example, here we have 2 potential candidates:

* Electrical Engineer, with skills: [Python, Math] - sum of skills values = 20
* Software Developer, with skills: [Python, Javascript, Postgres] - sum of skills values = 30

their order will be as follows:
1. Software Developer  (have total skill value score of 30)
2. Electrical Engineer (have total skill value score of 20, hence second place in the ranking)

**Note:** 
<em>currently all skills have been ranked the same, 
of course in the real world skill value may vary.</em>
